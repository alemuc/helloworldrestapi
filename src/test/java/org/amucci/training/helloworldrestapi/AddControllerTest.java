package org.amucci.training.helloworldrestapi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.amucci.training.helloworldrestapi.api.add.AddController;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AddControllerTest {

	@Test
	void adder() {
		assertEquals(new AddController().sayHello(1, 1).getNumber(), 2);
	}

}
