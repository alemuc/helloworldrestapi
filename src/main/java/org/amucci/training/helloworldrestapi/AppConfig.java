package org.amucci.training.helloworldrestapi;

import org.amucci.training.helloworldrestapi.api.pingpong.PulsarClientService;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
//@ComponentScan("org.amucci.training.helloworldrestapi")
//@PropertySource("classpath:database.properties")
public class AppConfig {

	@Autowired
	Environment environment;

//	private static final String URL = "url";
//	private static final String USER = "dbuser";
//	private static final String DRIVER = "driver";
//	private static final String PASSWORD = "dbpassword";

	@Bean("pulsarClientService")
	@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
	public PulsarClientService getPulsarClient() throws PulsarClientException {
		return new PulsarClientService();
	}

}
