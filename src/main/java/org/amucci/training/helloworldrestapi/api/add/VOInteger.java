package org.amucci.training.helloworldrestapi.api.add;

public class VOInteger {
	private final long number;

	public VOInteger(long number) {
		this.number = number;
	}

	public long getNumber() {
		return number;
	}

}
