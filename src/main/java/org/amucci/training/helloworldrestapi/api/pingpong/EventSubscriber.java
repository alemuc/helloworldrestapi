package org.amucci.training.helloworldrestapi.api.pingpong;

import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

import javax.annotation.PostConstruct;

import org.apache.pulsar.client.api.Consumer;
import org.apache.pulsar.client.api.Message;
import org.apache.pulsar.client.api.MessageId;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.shade.io.netty.util.internal.ConcurrentSet;
import org.apache.pulsar.shade.org.eclipse.util.ConcurrentHashSet;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
class EventSubscriberComponent implements DisposableBean {

    private volatile boolean someCondition = true;

    
    @Autowired
    PulsarClientService pulsarClientService;

    @PostConstruct
    private void postConstruct() {
    	// just to avoid to start a thread into the constructor
    	Runnable runnable = () -> { 
    		Consumer consumer;
			try {
				consumer = pulsarClientService.getPulsarClient().newConsumer()
				.topic("my-topic")
				.subscriptionName("my-subscription")
				.subscribe();
	    		
				while (someCondition) {
					System.out.println("Waiting for a message...");
					// Wait for a message
					Message msg = consumer.receive();

					try {
						// Do something with the message
						System.out.printf("Message received: Data:<%s>, EventTime:<%d>, PublishTime:<%d>, Key:<%s> \n", 
								new String(msg.getData()), 
								msg.getEventTime(), 
								msg.getPublishTime(),
								msg.getKey()
						);
						
	//					myConcurrentSet.add(msg.getMessageId());
						unblockByMessageId(msg.getMessageId());
						
						System.out.println("myConcurrentMap: " + myConcurrentMap);
						

						// Acknowledge the message so that it can be deleted by the message broker
						consumer.acknowledge(msg);
					} catch (Exception e) {
						// Message failed to process, redeliver later
						consumer.negativeAcknowledge(msg);
					}
				}
			} catch (PulsarClientException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

    	};
    	new Thread(runnable).start();
    }
    
    private ConcurrentHashMap<MessageId, CountDownLatch> myConcurrentMap = new ConcurrentHashMap<MessageId, CountDownLatch>();
    
    public synchronized void waitForMessageId(MessageId msgId) throws InterruptedException {
    	CountDownLatch done = new CountDownLatch(1);
    	myConcurrentMap.put(msgId, done);
    	System.out.println("waitForMessageId; myConcurrentSet: " + myConcurrentMap);
    	done.await();
    }
    
    private void unblockByMessageId(MessageId msgId) {
    	CountDownLatch done = myConcurrentMap.remove(msgId);
    	if (done != null) {
    		done.countDown();
    	}
    	System.out.println("unblockByMessageId; myConcurrentSet: " + myConcurrentMap);
    }
    
//    @Async
//    public CompletableFuture<MessageId> getMessageId(MessageId msgId) {
//    	myConcurrentSet.
//    	return CompletableFuture.completedFuture(msgId);
//    }
    
    @Override
    public void destroy(){
        someCondition = false;
    }

}