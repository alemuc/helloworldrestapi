package org.amucci.training.helloworldrestapi.api.pingpong;

import javax.annotation.PreDestroy;

import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

//@Service
public class PulsarClientService {
	
	private PulsarClient client = null;
	private Producer<byte[]> producer = null;
//	private Producer<byte[]> producer = null;
	
	public PulsarClientService() throws PulsarClientException {
		System.out.println("PulsarProducer constructor");
		client = PulsarClient.builder()
		        .serviceUrl("pulsar://localhost:6650")
		        .build();
		
		producer  = client.newProducer()
		        .topic("my-topic")
		        .create();
		
//		producer = client.newProducer()
//		        .topic("my-topic")
//		        .create();
	}
	
	public Producer getPulsarProducer() throws PulsarClientException {
		return producer;
	}
	
	public PulsarClient getPulsarClient() {
		return client;
	
//	public void pushMessage() throws PulsarClientException {
//		System.out.println("pushMessage");
//		producer.send("il mio messaggio".getBytes());
//	}
	
//	@PreDestroy
//	public void destroy() throws PulsarClientException {
//		System.out.println("Callback triggered - @PreDestroy.");
//		
////		producer.close();
//		client.close();
	}

}
