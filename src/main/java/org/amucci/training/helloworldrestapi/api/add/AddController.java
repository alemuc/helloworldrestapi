package org.amucci.training.helloworldrestapi.api.add;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AddController {


	@GetMapping("/api/v1/add")
	@ResponseBody
	public VOInteger sayHello(@RequestParam(name = "op1", required = true) long op1,
							 @RequestParam(name = "op2", required = true) long op2) {
		return new VOInteger(op1 + op2);
	}

}