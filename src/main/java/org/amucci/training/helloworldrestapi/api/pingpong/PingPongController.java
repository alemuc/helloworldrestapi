package org.amucci.training.helloworldrestapi.api.pingpong;

import org.amucci.training.helloworldrestapi.api.add.VOInteger;
import org.apache.pulsar.client.api.Message;
import org.apache.pulsar.client.api.MessageId;
import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.TypedMessageBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PingPongController {

	@Autowired
	private PulsarClientService pulsarProducer;

	@Autowired
	private EventSubscriberComponent eventSubscriber;
	
	
	@GetMapping("/api/v1/ping")
	@ResponseBody
	public String pingpong() throws PulsarClientException, InterruptedException {
//		pulsarProducer.pushMessage();
//		TypedMessageBuilder<byte[]> t = 
		MessageId msgId = pulsarProducer.getPulsarProducer().send("pong".getBytes());
				//.newMessage().value("pong".getBytes());
		
		eventSubscriber.waitForMessageId(msgId);
		
		// .send("pong".getBytes());
		return "pong <" + msgId + ">";
	}

	
}
